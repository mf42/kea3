
.. _installation:

Installation
------------

Kea3 required `Python <http://www.python.org/>`_ 3.6+. Apart from that, installation should be simple::

  pip install kea3


Use Postgres
~~~~~~~~~~~~

The default database is sqlite. If you want to use `Postgresql
<https://www.postgresql.org/>`_ instead, follow your system's
instructions to install. For example on Ubuntu and derivates::

  sudo apt install postgresql

Also, ensure the python bindings are installed::

  pip install psycopg2

Properly configure Postregsql for use with `Sqlalchemy
<http://docs.sqlalchemy.org/en/latest/dialects/postgresql.html>`_
(specifically `psycopg2
<http://docs.sqlalchemy.org/en/latest/dialects/postgresql.html#module-sqlalchemy.dialects.postgresql.psycopg2>`_)
(I expect it will need to listen to the network - see
`listen_addresses` in the postgresql configuration). Create a user and
database for kea, assign the correct permissions. Something like::

  $ sudo -u postgres psql

  postgres=# CREATE DATABASE keadb;
  postgres=# CREATE USER kea WITH ENCRYPTED PASSWORD 'keapass';
  postgres=# GRANT ALL PRIVILEGES ON DATABASE keadb TO kea;

Now, you need to tell kea to use this database by creating an
approriate `sqlalchemy connection string
<http://docs.sqlalchemy.org/en/latest/dialects/postgresql.html#module-sqlalchemy.dialects.postgresql.psycopg2>`_,
and tell kea of this::

  k3 conf set dbconnection 'postgresql+psycopg2://kea:KeaKea42@localhost/keadb'
