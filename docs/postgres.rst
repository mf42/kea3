




Installation steps on Arch::
  sudo pacman -Sy postgresql
  sudo -u postgres -i initdb --locale $LANG -E UTF8 -D /var/lib/postgres/data
  sudo systemctl start postgresql.service
  sudo systemctl enable postgresql.service


Then as user postgres::
  sudo -u postgres -i
  #create a user called kea
  createuser --interactive -P
  createdb -O kea kea

Lastly, make sure postgres listens, change `listen_adresses` in `/var/lib/postgres/data/postgresql.conf`
