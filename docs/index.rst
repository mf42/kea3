.. Kea documentation master file, created by
   sphinx-quickstart on Thu Aug  9 12:16:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Kea documentation
================================

Kea aims to be a lightweight system to track file and transactional
metadata and on top of this a simple scripting template system.

.. toctree::
   :caption: Table of contents
   :hidden:
   :maxdepth: 2

   self
   installation
   metadata
   source/modules

Introduction
------------

Kea aims to:

* **Track file metadata**: Kea tracks file metadata based on a file's
  `sha256` checksum. This means that if you delete a file and recreate
  it the metadata wil still be there. Similarly, if you have multiple
  copies of a file, all of them will share the metadata.
  :ref:`More..<metadata>`

* **Track relationships between files.**: Relationships between files
  are tracked by storing a record with the checksums of the related
  files. For example if one file is created from another file, Kea
  creates a record with the checksums the two files (and additional
  information). You can later query the database for any transaction
  related to a certain file. If you consequently store transactions
  (as part of your scripts), you will have full provenance. Again, by
  storing checksums, you can move files around without losing
  information.

* **Provide simple script templates**: File and transaction metadata
  can be stored individualy per file and transaction. However, to make
  things easier, Kea provides a simple templating system where you can
  build a template, inidicate what the in-, and output files are, and
  upon exeuction, Kea stores the transaction
  automatically. Additionally, the script engine provides the ability
  to run across many files at once.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
